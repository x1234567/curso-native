import { Component } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "listado",
    templateUrl: "./listado.component.html"
})

export class ListadoComponent {

    constructor(){
        //constructor del componente
    }

    ngOnInit(): void{
        //iniciar las propiedades del componente
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}