import { Component } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "vista",
    templateUrl: "./vista.component.html"
})

export class VistaComponent {

    constructor(){
        //constructor del componente
    }

    ngOnInit(): void{
        //iniciar las propiedades del componente
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}