import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row"> 
        <TextField[(ngModule)]="textFieldValue" hint="Ingresar texto..."></TextField>
        <Label *ngIf="texto.hasError('required')" text="*"></Label>
    </FlexboxLayout>
    <Button text="Buscar" (tap)="onButtonTap()"></Button>
    `
})

export class SearchFormComponet implements OnInit{
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;
    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }
    
    onButtonTap(): void{
        console.log(this.textFieldValue);
        if(this.textFieldValue.length > 2){
            this.search.emit(this.textFieldValue);
        }
    }
}