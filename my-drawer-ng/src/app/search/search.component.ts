import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Color, View } from "tns-core-modules/ui/page";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toast";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as SocialShare from "nativescript-social-share";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html"//,
    // providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<String>;
    @ViewChild("layout") layout: ElementRef;

    constructor(private noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.
        
    }

    ngOnInit(): void {
        // Init your component properties here.
        // console.log("hola mundo")
        // this.noticias.agregar("hola")
        // this.noticias.agregar("hola1")
        // this.noticias.agregar("hola2")
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.resultados.push("xxxxxxx");
            pullRefresh.refreshing = false;
        }, 2000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void{
        // console.dir(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string){
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscar ahora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscar ahora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
        // this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // })
        // )
    }
}
